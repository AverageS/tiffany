from flask import Flask, render_template, request
import os, utility, db



app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/create', methods=['POST'])
def create_note():
    data = request.form
    name = data['note-name']
    text = data['text']
    key = data['key']
    if os.path.isfile('notes/%s' % name):
        return "Note with this name already exists."
    with open('notes/%s' % name, 'w') as f:
        f.write(utility.encrypt(text, key))
    db.add_note(name, key)
    return "Note successfully created!"


@app.route('/note', methods=['POST'])
def render_note():
    data = request.form
    name = data['note-name']
    key = data['key']
    text = os.popen('cat "notes/%s"' % name).read()
    if db.get_key(name) == key:
        text = utility.decrypt(text, key)
    return text

import datetime, logging, sys, json_logging, flask
from contextlib import contextmanager

def fileno(file_or_fd):
    fd = getattr(file_or_fd, 'fileno', lambda: file_or_fd)()
    if not isinstance(fd, int):
        raise ValueError("Expected a file (`.fileno()`) or a file descriptor")
    return fd



@contextmanager
def stdout_redirected(to=os.devnull, stdout=None):
    if stdout is None:
       stdout = sys.stdout

    stdout_fd = fileno(stdout)
    # copy stdout_fd before it is overwritten
    #NOTE: `copied` is inheritable on Windows when duplicating a standard stream
    with os.fdopen(os.dup(stdout_fd), 'wb') as copied:
        stdout.flush()  # flush library buffers that dup2 knows nothing about
        try:
            os.dup2(fileno(to), stdout_fd)  # $ exec >&to
        except ValueError:  # filename
            with open(to, 'wb') as to_file:
                os.dup2(to_file.fileno(), stdout_fd)  # $ exec > to
        try:
            yield stdout # allow code to be run with the redirected stdout
        finally:
            # restore stdout to its previous value
            #NOTE: dup2 makes stdout_fd inheritable unconditionally
            stdout.flush()
            os.dup2(copied.fileno(), stdout_fd)  # $ exec >&copied


if __name__ == "__main__":

    logging.basicConfig(filename='error.log',level=logging.DEBUG, force=True)
    json_logging.init_flask(enable_json=True)
    logger = logging.getLogger('json_logging')
    json_logging.init_request_instrument(app)
    import os
    import sys

    stdout_fd = sys.stdout.fileno()
    with open('/var/log/tiffany.log', 'w') as f, stdout_redirected(f):
        app.run(host='0.0.0.0', use_reloader=False)