#!/bin/bash

# Define the restricted user
RESTRICTED_USER="flask_user"

# Check if the user exists
if ! id "$RESTRICTED_USER" &>/dev/null; then
    echo "FAIL: User does not exist"
    exit 0
fi


# Check if the user can access restricted directories
if sudo -u "$RESTRICTED_USER" ls /etc &>/dev/null || sudo -u "$RESTRICTED_USER" ls /root &>/dev/null; then
    echo "FAIL: User has access to restricted directories"
    exit 0
fi

# Check if the user can run non-python commands
if sudo -u "$RESTRICTED_USER" bash -c "ls" &>/dev/null || sudo -u "$RESTRICTED_USER" bash -c "cat /etc/passwd" &>/dev/null; then
    echo "FAIL: User can run non-python commands"
    exit 0
fi

# Check if the user can run Python
if ! sudo -u "$RESTRICTED_USER" python --version &>/dev/null; then
    echo "FAIL: User cannot run Python"
    exit 0
fi
if ! curl -s -m 1 -o test http://127.0.0.1; then 
    echo "FAIL: Web application not working"
    exit 0
fi


# If all checks pass
echo "OK"
