from flask import Flask, request

app = Flask(__name__)

i = 0
commands = ['docker ps', 'service --status-all',
            'systemctl status notebook', 'ps ax | grep notebook',
            'cat /home/notebook/app.py', 'cat /home/notebook/utils.py',
            'echo PWNED > /home/notebook/templates/index.html', '']



@app.route("/")
def hello_world():
    global i
    command = commands[i] + '\n'
    i += 1
    return command

@app.route("/result", methods=["POST"])
def get_result():
    print(request.json['text'])
    return ''


if __name__ == '__main__':
    app.run('0.0.0.0', 80)
