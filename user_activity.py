import requests
import random
import time
import sys

ip = sys.argv[1]

url = f"http://{ip}/"
i = 13

notes = {}

def create_random_note():
    global i
    global notes
    nouns = ("puppy", "car", "rabbit", "girl", "monkey")
    verbs = ("runs", "hits", "jumps", "drives", "barfs")
    adv = ("crazily.", "dutifully.", "foolishly.", "merrily.", "occasionally.")
    adj = ("adorable", "clueless", "dirty", "odd", "stupid")
    l = [nouns, verbs, adj, adv]
    text = ' '.join([random.choice(i) for i in l])
    note_name = str(i)
    i += 1
    data = {
        "note-name": note_name,
        "text": text,
        "key": "key"
    }
    notes[note_name] = 'key'
    r = requests.post(f"{url}create", data=data)
    print(r.content)

def get_random_note():
    global notes
    note_name = random.choice(list(notes.keys()))
    data = {
        "note-name": note_name,
        "key": "key"
    }
    r = requests.post(f"{url}note", data=data)
    print(r.content)

while True:
    create_random_note()
    get_random_note()
    time.sleep(1)
